Vim config
===

# Install
* `git clone https://gitlab.com/efugier/vim-config.git ~/.vim`
* Run the `setup.sh` script.
* Do `:PlugInstall` to install plugins.
* Additions can be made in a `my_config.vim` file to keep things neat.

# Keeping it up to date
* Update plugins: `:PlugUpdate`
* Update the config: `git pull`

# Highlights
* `<Space>` is the leader key.
* `<++>` are placeholders, press `<Space><Space>` to go to the next placeholder
* `**` is appended to the path to make the search fuzzy by default.
    * For this to be effective, leave the current directory qt the root of the project.
* `%%` expands the current file's directory in command mode.
* See `:help netrw-browse-maps` for file browsing.
* See `:help ins-completion` for autocompletion.
* `,` is for snippets.
    * `,html`, `,react`, `,freact`
    * Snippets can be added in the snippets folder, see [config.vim](./config.vim) to create new shortcuts.
* Closing a buffer will not close the window.
* `<leader>bc` clean the buffer list (closes the unused ones).

## Some mappings and commands
* `:MakeTags` generates tags.
    * Use [universal-ctags](https://github.com/universal-ctags/ctags), a maintained version of ctags.
* `<leader>s` for quick save.
* `<leader>sq` for quick save and quit.
* `<C-z>,` to trigger emmet completion.
* `<F5>` removes all trailing whitespaces.
* `<leader>g` `s c p a` to git status, commit, push, amend.
* `<leader><CR>` to remove highlights.
* `<leader>t` `n c o l` for tab new, close, only, last.
* `<leader>f` to `:find`.
* `<leader>b` `p n` to `:b` `:bprev` `:bnext` .
* `<leader>w` to `<C-w>` for window management.
* `*` and `#` to search for the current selection in visual mode.
* `:set bl` or `buflisted` to make netrw buffer persistent.

More info in [config.vim](./config.vim) through comments.

# Plugins
* [vim-commentary](https://github.com/tpope/vim-commentary)
* [vim-fugitive](https://github.com/tpope/vim-fugitive)
* [vim-signify](https://github.com/mhinz/vim-signify)
* [vim-gutentags](https://github.com/ludovicchabant/vim-gutentags)
* [vim-repeat](https://github.com/tpope/vim-repeat)
* [vim-surround](https://github.com/tpope/vim-surround)
* [lightline](https://github.com/itchyny/lightline.vim)
* [emmet-vim](https://github.com/mattn/emmet-vim)
* [ale](https://github.com/w0rp/ale)

## Language specifics
* [vim-javascipt](https://github.com/pangloss/vim-javascript)
* [vim-jsx](https://github.com/mxw/vim-jsx)
* [vim-scala](https://github.com/derekwyatt/vim-scala)
* [vim-markdown](https://github.com/plasticboy/vim-markdown')
* [php.vim](https://github.com/StanAngeloff/php.vim)

Plugins are installed using [vim-plug](https://github.com/junegunn/vim-plug).

# Tips
* `:b` to change buffer, `ls` to display them all.
* `:find` to find a file.
* `vit` or `vat` to select inner/outter tag, then `o` or `O` to go to top/bottom.
* Add folder you want to ignore on find (such as node_modules to the wildignore variable like in [config.vim](./config.vim)).
* `<C-o>`, `<C-i>` to jump between locations (previous, next).
* `'.`, `\`.` to jump last edit line / exact location.
* `g;`, `g,` to jump between change locations.
* `vim scp://user@myserver[:port]//path/to/file.txt` to edit a remote file

## Usefull ressources
* [Nice infos](https://www.cs.oberlin.edu/~kuperman/help/vim/home.html)
* [Tabs, windows and buffers](https://stackoverflow.com/questions/26708822/why-do-vim-experts-prefer-buffers-over-tabs)
* [Most productive shortcuts with vim](https://stackoverflow.com/questions/1218390/what-is-your-most-productive-shortcut-with-vim)
* [Vim for php](https://github.com/roxma/ncm-phpactor)
* [Vim sessions](https://bocoup.com/blog/sessions-the-vim-feature-you-probably-arent-using)
* [About netrw](https://shapeshed.com/vim-netrw/)

