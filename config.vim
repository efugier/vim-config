set nocompatible
let mapleader = " "

set belloff=all
syntax enable

" switch syntax highlighting on
syntax on

" enable file type detection and do language-dependent indenting.
filetype plugin indent on

"html and css indent
:let g:html_indent_script1 = "inc"
:let g:html_indent_style1 = "inc"

set autoread

" make backspace behave in a sane manner.
set backspace=indent,eol,start

" traverse line breaks
set whichwrap=b,s,<,>,[,]

" tab are only for indentation"
" set smarttab
" expand tabs into spaces
set expandtab
" when using the >> or << commands, shift lines by X spaces
set shiftwidth=2
" set tabs to have X spaces
set softtabstop=2
" indent when moving to the next line while writing code
set autoindent

" show the matching part of the pair for [] {} and ()
set showmatch

" enable all Python syntax highlighting features
let python_highlight_all = 1


" line numbering
set number
set relativenumber
" lines to the cursor when moving verticaly
set scrolloff=2
" no redraw on macros
set lazyredraw
" line handling
set linebreak
set breakindent
set wrap

" return to last edit position when opening files (You want this!)
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif
" remember info about open buffers on close
set viminfo^=%

""" FUZZY PATH:
set path+=**
" display all the matching files when tab completing
set wildmenu
" ingnore some files
set wildignore=*.o,*~,*.pyc
if has("win16") || has("win32")
    set wildignore+=.git\*,.hg\*,.svn\*,*\node_modules\*
else
    set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/.DS_Store,*/node_modules/*
endif

" case-sensitive search only when search contains upper-case chars
set ignorecase
set smartcase
" search as you type
set incsearch

""" AUTOCOMPLETE:
" see ins-completion for help
" ^x^n for JUST this file
" ^x^f for filenames (works with the path)
" ^x^] for tags
" ^n   for anything
" ^p and ^n to jump back and forth

""" FILE BROWSING:
" netrw config
let g:netrw_liststyle=3     " tree view
let g:netrw_banner=0        " disable info banner
let g:netrw_browse_split=4  " open in prior window
let g:netrw_winsize = 17    " size of 25%
let g:netrw_altv=1          "open splits to the right
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide.=',\(^\|\s\s\)\zs\.\S\+'
" Show tree
nmap <leader>ts :Vexplore<CR>:set buflisted<CR>
" start netrw with vim
" augroup ProjectDrawer
"   autocmd!
"   autocmd VimEnter * :Vexplore
" augroup END
" see netrw-browse-maps

""" NOBACKUP:
set nobackup
set nowb
set noswapfile


""" HISTORY:
:set history=1000


""""""""""""""""""""""""
""" MAPPING & COMMANDS
""""""""""""""""""""""""


""" Open a new terminal in current direcotry
" /!\ Requiers $MYTERM to be set
" /!\ The synthax is made for alacritty, might not be compatible with every terminal
nmap <leader>th :!/bin/bash -c $MYTERM --working-directory $(pwd) </dev/null &>/dev/null &<CR><CR>

" jump to next placeholder
nmap <leader><leader> /<++><CR>:noh<CR>c4l

" stupid html tag completion (changes the default register though)
"imap <C-j> ><Esc>?<<CR>ye/><CR>:noh<CR>a</><Esc>PF<"_xF<i

" more intutive bindings
nmap Y y$

" quicksave
nmap <leader>s :w<CR>
nmap <leader>sq :wq<CR>
" save as temporary file
nmap <leader>st :w /tmp/

"yank everything
nmap <leader>ya gg0vG$"+y<C-o><C-o>
" yank everything and force quit
nmap <leader>yq gg0vG$"+y:q!<CR>

" prevent vim from clearing the clipboard on leave
autocmd VimLeave * call system("xsel -ib", getreg('+'))

" set highlights when searching
set hlsearch
" disable highlights
map <leader><CR> :noh<CR>

" tab management
map <leader>tn :tabnew<CR>
map <leader>tc :tabclose<CR>
map <leader>to :tabonly<CR>
" tl to switch with the last used tab
let g:lasttab = 1
nmap <Leader>tl :exe "tabn ".g:lasttab<CR>
au TabLeave * let g:lasttab = tabpagenr()

" toggle spell checking
map <leader>ssf :setlocal spell! spelllang=fr<CR>
map <leader>sse :setlocal spell! spelllang=en<CR>

" buffer management
map <leader>b :b<Space>
map <leader>bp :bprev<CR>
map <leader>bn :bnext<CR>
map <leader>bd :Bclose<CR>
map <Leader>bc :Bclean<CR>
" don't close window, when deleting a buffer
command! Bclose call <SID>BufcloseCloseIt()
" remove all unused buffers (i.e. not in a window or a tab)
command! Bclean call DeleteInactiveBufs()

" window management
map <leader>w <C-w>
" window resizing
" nnoremap <silent> <Leader>wh :exe "resize " . (winheight(0) * 1/10)<CR> 
" nnoremap <silent> <Leader>wH :exe "resize " . (winheight(0) * 11/10)<CR>
nnoremap <silent> <Leader>ww :exe "vertical resize " . (winwidth(0) * 11/10)<CR>
nnoremap <silent> <Leader>wW :exe "vertical resize " . (winwidth(0) * 9/10)<CR> 

" find files
map <leader>f :find<Space>

" edit files
map <leader>e :e<Space>
" edit in the current from buffer's directory
" from http://vimcasts.org/episodes/the-edit-command/
cnoremap %% <C-R>=fnameescape(expand('%:h')).'/'<cr>

" run commands
cnoremap !! echo system('')<left><left>

" visual mode pressing * or # searches for the current selection
vnoremap <silent> * :<C-u>call VisualSelection('', '')<CR>/<C-R>=@/<CR><CR>
vnoremap <silent> # :<C-u>call VisualSelection('', '')<CR>?<C-R>=@/<CR><CR>

""" TAG JUMPING:
command! MakeTags !ctags -R .
" consider using universal ctags, a mainteined version of ctags
" use ^] to jump to the tag under the cursor
" use g^] for ambiguous tags
" use ^t to jump back un the tag stack

" clear registers
command! ClearRegisters for i in range(34,122) | silent! call setreg(nr2char(i), []) | endfor

" remove trailing whitespaces
nnoremap <F5> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR>


""" CURSOR CHANGE:
if exists('$TMUX')
    let &t_SI = "\ePtmux;\e\e[5 q\e\\"
    let &t_EI = "\ePtmux;\e\e[2 q\e\\"
else
    let &t_SI = "\e[5 q"
    let &t_EI = "\e[2 q"
endif


""""""""""""""""""""""""
""" HELPER FUNCTIONS
""""""""""""""""""""""""

function! <SID>BufcloseCloseIt()
    let l:currentBufNum=bufnr("%")
    let l:alternateBufNum=bufnr("#")
    if buflisted(l:alternateBufNum)
        buffer #
    else
        bnext
    endif
    if bufnr("%") == l:currentBufNum
        new
    endif
    if buflisted(l:currentBufNum)
        execute("bdelete! ".l:currentBufNum)
    endif
endfunction

function! DeleteInactiveBufs()
  "From tabpagebuflist() help, get a list of all buffers in all tabs
  let tablist = []
  for i in range(tabpagenr('$'))
    call extend(tablist, tabpagebuflist(i + 1))
  endfor
  "Below originally inspired by Hara Krishna Dara and Keith Roberts"http://tech.groups.yahoo.com/group/vim/message/56425
  let nWipeouts = 0
  for i in range(1, bufnr('$'))
    if bufexists(i) && !getbufvar(i,"&mod") && index(tablist, i) == -1
      "bufno exists AND isn't modified AND isn't in the list of buffers open in windows and tabs
      silent exec 'bwipeout' i
      let nWipeouts = nWipeouts + 1
    endif
  endfor
  echomsg nWipeouts . ' buffer(s) wiped out'
endfunction

function! VisualSelection(direction, extra_filter) range
    let l:saved_reg = @"
    execute "normal! vgvy"

    let l:pattern = escape(@", "\\/.*'$^~[]")
    let l:pattern = substitute(l:pattern, "\n$", "", "")

    if a:direction == 'gv'
        call CmdLine("Ack '" . l:pattern . "' " )
    elseif a:direction == 'replace'
        call CmdLine("%s" . '/'. l:pattern . '/')
    endif

    let @/ = l:pattern
    let @" = l:saved_reg
endfunction

