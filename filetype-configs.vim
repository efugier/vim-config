""" SNIPPETS:
nnoremap <leader>,html :-1read $HOME/.vim/snippets/skeleton.html<CR>
nnoremap <leader>,react :-1read $HOME/.vim/snippets/skeleton.react-component.jsx<CR>
nnoremap <leader>,freact :-1read $HOME/.vim/snippets/skeleton.functional-react-component.jsx<CR>

""" FILETPYPE SETTINGS:
" cpp
augroup cpp_settings " {
  " autocmd BufRead,BufNewFile *.c,*.cpp,*.h 
  autocmd!
	" indentation
	autocmd BufRead,BufNewFile *.c,*.cpp,*.h setlocal expandtab shiftwidth=4 softtabstop=4
	" snippets
        autocmd BufRead,BufNewFile *.c,*.cpp,*.h nnoremap <buffer> <leader>,f :-1read $HOME/.vim/snippets/function.cpp<CR>hi
augroup END "}

""" FILETPYPE SETTINGS:
" cpp
augroup cpp_settings " {
  " autocmd BufRead,BufNewFile *.c,*.cpp,*.h 
  autocmd!
	" indentation
	autocmd BufRead,BufNewFile *.c,*.cpp,*.h setlocal expandtab shiftwidth=2 softtabstop=2
	" snippets
        autocmd BufRead,BufNewFile *.c,*.cpp,*.h nnoremap <buffer> <leader>,f :-1read $HOME/.vim/snippets/function.cpp<CR>hi
augroup END "}

" javascript
augroup javascript_settings " {
  " autocmd BufRead,BufNewFile *.js 
  autocmd!
  " indentation
  autocmd BufRead,BufNewFile *.js setlocal expandtab shiftwidth=4 softtabstop=4
  " snippets
  autocmd BufRead,BufNewFile *.js nnoremap <buffer> <leader>,f :-1read $HOME/.vim/snippets/function.js<CR>
  autocmd BufRead,BufNewFile *.js nnoremap <buffer> <leader>,lf :-1read $HOME/.vim/snippets/function.lambda.js<CR>
augroup END "}

" php
augroup php_settings " {
  " autocmd BufRead,BufNewFile *.php
  autocmd!
  " indentation
  autocmd BufRead,BufNewFile *.php setlocal expandtab shiftwidth=4 softtabstop=4
  autocmd FileType php setlocal omnifunc=phpactor#Complete
augroup END "}

" go
augroup go_settings " {
  " autocmd BufRead,BufNewFile *.go
  autocmd!
  " indentation
  autocmd BufRead,BufNewFile *.go setlocal shiftwidth=4 softtabstop=4 tabstop=4
augroup END "}
