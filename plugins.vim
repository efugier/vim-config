""""""""""""""""""""""""
""" PLUGINS
""""""""""""""""""""""""

" Installation
call plug#begin('~/.vim/plugged')

" Misc
Plug 'https://github.com/tpope/vim-commentary'
Plug 'https://github.com/tpope/vim-fugitive'
Plug 'https://github.com/mhinz/vim-signify'
Plug 'https://github.com/tpope/vim-repeat'
Plug 'https://github.com/tpope/vim-surround'
Plug 'https://github.com/ludovicchabant/vim-gutentags'
Plug 'https://github.com/itchyny/lightline.vim'
Plug 'https://github.com/mattn/emmet-vim'
Plug 'https://github.com/w0rp/ale'
" Markdown
Plug 'https://github.com/plasticboy/vim-markdown'
" Javascript
Plug 'https://github.com/pangloss/vim-javascript'
Plug 'https://github.com/mxw/vim-jsx'
" Web Components
Plug 'https://github.com/azakus/vim-webcomponents'
" Scala
Plug 'https://github.com/derekwyatt/vim-scala'
" PHP
Plug 'https://github.com/StanAngeloff/php.vim'
" Go
Plug 'https://github.com/fatih/vim-go', { 'do': ':GoUpdateBinaries' }
" Rust
Plug 'https://github.com/rust-lang/rust.vim'

call plug#end()

""" ALE:
" Disable Ale by default
autocmd VimEnter * ALEDisable
" Jump to Next/Previous error
nmap <silent> <leader>aj :ALENext<CR>
nmap <silent> <leader>ak :ALEPrevious<CR>
" Less aggressive than the default '>>'
" let g:ale_sign_error = '●'
" let g:ale_sign_warning = '.'

" Easier on CPU
let g:ale_lint_on_save = 1
let g:ale_lint_on_text_changed = 0

""" LIGHTLINE:
" fix colors
if !has('gui_running')
  set t_Co=256
endif
" config
set laststatus=2
set noshowmode
let g:lightline = {
      \ 'colorscheme': 'nord',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'fugitive#head'
      \ },
      \ }

""" FUGITIVE:
" git commands on <leader>g
map <leader>gs :Gstatus<CR>
map <leader>gc :Gcommit<CR>
map <leader>gd :Gdiff<CR>
map <leader>gp :Gpush<CR>
map <leader>ga :Gcommit --amend<CR>
