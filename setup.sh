#!/bin/sh
set -e

cd ~/.vim

echo '
source ~/.vim/config.vim
source ~/.vim/plugins.vim
source ~/.vim/filetype-configs.vim

try
source ~/.vim/my_config.vim
catch
endtry
' > ~/.vimrc

echo "Installed the configuration successfully! Run :PlugInstall and enjoy =)"
